<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cpf', 50);
            $table->string('nome', 50);
            $table->string('celular', 15);
            $table->string('rg', 20);
            $table->string('nascimento', 15);
            $table->string('email', 60);
            $table->string('cidade', 50);
            $table->string('bairro', 50);
            $table->string('endereco', 60);
            $table->string('genero', 30);
            $table->string('renda', 20);
            $table->string('corOuRaca', 50);
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
