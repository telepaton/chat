<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'cpf' => str_random(10),
            'nome' => str_random(10),
	        'celular' => str_random(10),
	        'rg' => str_random(10),
	        'nascimento' => str_random(10),
            'email' => str_random(10).'@gmail.com',
	        'cidade' => str_random(10),
	        'bairro' => str_random(10),
	        'endereco' => str_random(10),
	        'genero' => str_random(10),
	        'renda' => str_random(10),
	        'corOuRaca' => str_random(10),
	        'remember_token' => str_random(10),
        ]);
    }
}
