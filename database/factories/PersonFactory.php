<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Person::class, function (Faker $faker) {

    return [
        'cpf' => $faker->sentence(12, true),
        'nome' => $faker->name,
        'celular' => $faker->phoneNumber ,
        'rg' => $faker->randomNumber($nbDigits = NULL, $strict = false),
        'nascimento' => $faker->dateTimeThisCentury->format('Y-m-d'),
        'email' => $faker->unique()->safeEmail,
        'cidade' => $faker->streetAddress,
        'bairro' => $faker->streetAddress,
        'endereco' => $faker->streetAddress,
        'genero' => $faker->sentence(12, true),
        'renda' => $faker->randomNumber($nbDigits = NULL, $strict = false),
        'corOuRaca' => $faker->sentence(12, true),
        'remember_token' => str_random(10),
    ];

});
