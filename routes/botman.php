<?php
use App\Http\Controllers\BotManController;

$botman = resolve('botman');

$botman->hears('Oi', function ($bot) {
    $bot->reply('Bem Vindo! Aratu é o Canal, o seu canal! Qual o seu nome?');
});
$botman->hears('Start conversation', BotManController::class.'@startConversation');

$botman->hears('Meu nome é {name}', function ($bot, $name) {
    $bot->reply('Prazer '.$name. ',nós conte qual o seu programa favorito');
});

$botman->fallback(function($bot) {
    $bot->reply('Desculpe, não conseguimos entender');
});

// $botman->ask('Quem vai vencer? Bahia ou Vitória', function($answer) {
// 	$bot->firstname = $answer->getText();
// });

// $botman->hears('Hello', function($bot) {
// 	$user = $bot->getUser();
// 	$bot->reply('Hello '.$user->getFirstName().' '.$user->getLastName());
// 	$bot->reply('Your username is: '.$user->getUsername());
// 	$bot->reply('Your ID is: '.$user->getId());
// });