<?php


Route::group(array('prefix' => 'api'), function()
{

  Route::get('/', function () {
      return response()->json(['message' => 'Chat API', 'status' => 'Connected']);;
  });

  Route::resource('users', 'UsersController');
});

Route::get('/', function () {
    return redirect('api');
});