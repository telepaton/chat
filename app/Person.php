<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
      protected $fillable = [
        'cpf','nome', 'celular', 'rg','nascimento','email','cidade','bairro','endereco','genero','renda','corOuRaca',
    ];
}
