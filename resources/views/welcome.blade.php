@extends("layouts.app")

@section('title')
    Home
@endsection

@section ('content')

    
<div class="container text-center home">
    <div class="content">
        <div class="container">
      <div class="circle-wrapper">
        <div class="circle-zero"><b style="font-size: 20px" class="text-dark">Aratu ON</b></div>
        <div class="circle-one"></div>
        <div class="circle-two"></div>
        <div class="circle-three"></div>
        <div class="circle-shadow"></div>
      </div>
    </div>

        <div class="">
            <a href="/botman/tinker" class="px-2"><b>Geral</b></a>
            <a href="https://botman.io/docs" target="_blank" class="px-2"><b>Música</b></a>
            <a href="https://twitter.com/botman_io" target="_blank" class="px-2"><b>Futebol</b></a>
            <a href="https://rauchg-slackin-jtdkltstsj.now.sh/" target="_blank" class="px-2"><b>Culinária</b></a>
            <a href="https://github.com/botman/botman" target="_blank" class="px-2"><b>Bairros</b></a>
        </div>
    </div>
</div>
@endsection
