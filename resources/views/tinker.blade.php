@extends("layouts.app")

@section('title')
    Home
@endsection

@section ('content')

    <div class="container text-center">
        <div class="content" id="app">
        	<p>Ao enviar mensagens você autoriza o uso dos dados enviados pela TV Aratu, seus dados não serão divulgados para terceiros</p>
            <botman-tinker api-endpoint="/botman"></botman-tinker>
        </div>
    </div>

@endsection


