<!DOCTYPE html>
<html>
<head>

    @section('head')
        <meta charset="utf-8">
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robot" content="All">
        <meta name="rating" content="general">
        <meta name="distribution" content="global">
        <meta name="language" content="pt-br">
        <meta name="msapplication-TileColor" content="#2b5797">
        <meta name="theme-color" content="#ffffff">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" media ="screen" title="no title">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" media ="screen" title="no title">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    @show

    <title>AratuON | @yield('title')</title>
    
</head>
<body>
    <div id="app">
        @section('navbar')
            @include('includes.navbar')
        @show

        @yield('content')


    </div>

    <!-- Scripts -->

    @section('script')
        @include('includes.script')
    @show

</body>
</html>
